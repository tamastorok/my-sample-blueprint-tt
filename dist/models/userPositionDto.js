"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserPositionDto = void 0;
class UserPositionDto {
    constructor(userAddress, blueprintKey, chainId, positionIdentifier, operation, transactionHash, blockNumber) {
        this.userAddress = userAddress;
        this.blueprintKey = blueprintKey;
        this.chainId = chainId;
        this.positionIdentifier = positionIdentifier;
        this.operation = operation;
        this.transactionHash = transactionHash;
        this.blockNumber = blockNumber;
    }
}
exports.UserPositionDto = UserPositionDto;
